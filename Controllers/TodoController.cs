﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TodoApp.Models;
using TodoApp.Repositories;

namespace TodoApp.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TodoController : ControllerBase
    {
        private readonly ILogger<TodoController> _logger;

        public TodoController(ILogger<TodoController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<Todo> GetTodos(string userId)
        {
            var repo = new TodoRepository();

            return repo.GetTodos(userId).Result;
        }

        public Task PostTodo(Todo todo)
        {
            var repo = new TodoRepository();

            if (string.IsNullOrEmpty(todo.RecurrenceRules))
            {
                return repo.CreateTodo(todo);
            }

            var untilRegex = new Regex(@"until .+");
            var untilMatch = untilRegex.Match(todo.RecurrenceRules);
            DateTime? untilDateTime = null;

            if (untilMatch.Success)
            {
                untilDateTime = DateTime.ParseExact(untilMatch.Value, "yyyyMMdd", null, DateTimeStyles.None);
            }

            var dailyRegex = new Regex(@"(\d+)d");
            var dailyMatch = dailyRegex.Match(todo.RecurrenceRules);
            if (dailyMatch.Success)
            {
                var tempDate = DateTime.Now;

                while (tempDate <= untilDateTime)
                {
                    repo.CreateTodo(todo);
                    tempDate.AddDays(double.Parse(dailyMatch.Value));
                }
            }

            var weeklyRegex = new Regex(@"(\d+)w");
            var weeklyMatch = weeklyRegex.Match(todo.RecurrenceRules);
            if (weeklyMatch.Success)
            {
                var tempDate = DateTime.Now;

                while (tempDate <= untilDateTime)
                {
                    repo.CreateTodo(todo);
                    tempDate.AddDays(7 * double.Parse(dailyMatch.Value));
                }
            }

            return Task.FromResult(new OkResult());
        }
    }
}
