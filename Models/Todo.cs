﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TodoApp.Models
{
    public class Todo
    {
        public string Description { get; set; }

        public DateTime DueOn { get; set; }

        public string Id { get; set; }

        public string RecurrenceRules { get; set; }
    }
}
