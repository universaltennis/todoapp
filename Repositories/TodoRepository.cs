﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using TodoApp.Models;

namespace TodoApp.Repositories
{
    public class TodoRepository
    {
        public Task<List<Todo>> GetTodos(string userid)
        {
            var connection = new SqlConnection("connectionString");
            var result = connection.QueryAsync<Todo>("select * from dbo.Todos where userId=" + userid).Result.ToList();

            if (!result.Any())
            {
                throw new Exception("404");
            }

            return Task.FromResult(result);
        }

        public Task CreateTodo(Todo todo)
        {
            var connection = new SqlConnection("connectionString");

            connection.ExecuteAsync($"insert into dbo.Todos (Description), (DueOn) values ({todo.Description}), ({todo.DueOn})");

            return Task.CompletedTask;
        }
    }
}
